const start = document.getElementById('start')
const rock = document.getElementById('rock')
const paper = document.getElementById('paper')
const scissors = document.getElementById('scissors')
const game = document.getElementById('game')
const rules = document.getElementById('rules')
const input = document.getElementById('input')
const gameButtons = document.getElementById('gameButtons')
const reset = document.getElementById('reset')

let round =0;
let you = 0;
let opponent = 0;

start.addEventListener('click', () => {
    game.style.display = 'block'
    rules.style.display = 'none'
})


const oponentTurn = () => {
    let options = [
        'rock',
        'paper',
        'scissors'
    ]
    let random = Math.floor(Math.random() * 3)
    return options[random]
}

const createEl = (answer) => {
    let p = document.createElement(p)
    p.innerHTML = answer
    input.appendChild(p)
}

const checkForWin = () => {
       if(round >= 3){ let answer
        if (you > opponent) {
            answer = 'you`ve won'
        } else if (you < opponent){
            answer = 'your opponent wins'
        } else if (you === opponent){
            answer = 'it is a tie'
        }
        createEl(answer)
        gameButtons.style.display = 'none'
        reset.style.display = 'block'}
}

reset.addEventListener('click', () => {
    round = 0;
    you = 0;
    opponent = 0;
    gameButtons.style.display = 'flex'
    game
    reset.style.display = 'none'
    input.innerHTML = ''
})
paper.addEventListener('click', () => {
    let answer;
    let answerOponent = oponentTurn()
    if( answerOponent === 'scissors') {
            round++
            opponent++
            answer = `Round ${round}, paper vs. scissors, You've lost!`
        } else if( answerOponent === 'paper') {
            round++
            opponent++
            you++
            answer = `Round ${round}, paper vs. paper, it's a Tie!`
        } else if( answerOponent === 'rock') {
            round++
            you++
            answer = `Round ${round}, paper vs. rock, You've won!`
        }

        createEl(answer)
        checkForWin()
})
rock.addEventListener('click', () => {
    let answer;
    let answerOponent = oponentTurn()
    if( answerOponent === 'scissors') {
            round++
            answer = `Round ${round}, rock vs. scissors, You've won!`
        } else if( answerOponent === 'paper') {
            round++
            opponent++
            answer = `Round ${round}, rock vs. paper, You've lost!`
        } else if( answerOponent === 'rock') {
            round++
            opponent++
            you++
            answer = `Round ${round}, rock vs. rock, it's a Tie!`
        }

        createEl(answer)
        checkForWin()
})


scissors.addEventListener('click', () => {
    let answer;
    let answerOponent = oponentTurn()  
        if( answerOponent === 'scissors') {
            round++
            opponent++
            you++
            answer = `Round ${round}, scissors vs. scissors, it's a Tie!`
        } else if( answerOponent === 'paper') {
            round++
            you++
            answer = `Round ${round}, scissors vs. paper, You've won!`
        } else if( answerOponent === 'rock') {
            round++
            opponent++
            answer = `Round ${round}, scissors vs. rock, You've lost!`
        }

        createEl(answer)
        checkForWin()
})

