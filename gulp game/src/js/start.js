const start = document.getElementById('start')
const rock = document.getElementById('rock')
const paper = document.getElementById('paper')
const scissors = document.getElementById('scissors')
const game = document.getElementById('game')
const rules = document.getElementById('rules')
const input = document.getElementById('input')
const gameButtons = document.getElementById('gameButtons')
const reset = document.getElementById('reset')

let round =0;
let you = 0;
let opponent = 0;

start.addEventListener('click', () => {
    game.style.display = 'block'
    rules.style.display = 'none'
})


const oponentTurn = () => {
    let options = [
        'rock',
        'paper',
        'scissors'
    ]
    let random = Math.floor(Math.random() * 3)
    return options[random]
}

const createEl = (answer) => {
    let p = document.createElement(p)
    p.innerHTML = answer
    input.appendChild(p)
}

const checkForWin = () => {
       if(round >= 3){ let answer
        if (you > opponent) {
            answer = 'you`ve won'
        } else if (you < opponent){
            answer = 'your opponent wins'
        } else if (you === opponent){
            answer = 'it is a tie'
        }
        createEl(answer)
        gameButtons.style.display = 'none'
        reset.style.display = 'block'}
}

reset.addEventListener('click', () => {
    round = 0;
    you = 0;
    opponent = 0;
    gameButtons.style.display = 'flex'
    game
    reset.style.display = 'none'
    input.innerHTML = ''
})