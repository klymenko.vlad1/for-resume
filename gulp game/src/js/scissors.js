scissors.addEventListener('click', () => {
    let answer;
    let answerOponent = oponentTurn()  
        if( answerOponent === 'scissors') {
            round++
            opponent++
            you++
            answer = `Round ${round}, scissors vs. scissors, it's a Tie!`
        } else if( answerOponent === 'paper') {
            round++
            you++
            answer = `Round ${round}, scissors vs. paper, You've won!`
        } else if( answerOponent === 'rock') {
            round++
            opponent++
            answer = `Round ${round}, scissors vs. rock, You've lost!`
        }

        createEl(answer)
        checkForWin()
})