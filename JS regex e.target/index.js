/* eslint-disable no-magic-numbers */
/* START TASK 1: Your code goes here */

let cell = document.getElementById('task1');
let tobluecell1 = document.getElementById('blueCell1');
let tobluecell2 = document.getElementById('blueCell2');
let tobluecell3 = document.getElementById('blueCell3');
let specialClick = document.getElementById('specialCell');
cell.onclick = (e) => {
 if(e.target !== tobluecell1 && e.target !== tobluecell2 && e.target !== tobluecell3 && e.target !== specialClick) {
 e.target.classList.toggle('yellow') 
} 
};
tobluecell1.onclick = () =>
  Array.from(document.getElementsByClassName('row-blue1')).forEach((e) =>
    e.classList.toggle('blue')
  );
tobluecell2.onclick = () =>
Array.from(document.getElementsByClassName('row-blue2')).forEach((e) =>
  e.classList.toggle('blue')
);
tobluecell3.onclick = () =>
Array.from(document.getElementsByClassName('row-blue3')).forEach((e) =>
  e.classList.toggle('blue')
);
specialClick.onclick = () => 
Array.from(document.getElementsByTagName('td')).forEach((e) =>
e.classList.toggle('green')
);

/* END TASK 1 */

/* START TASK 2: Your code goes here */


let input = document.getElementById('inputNumber')
let correct = document.getElementById('correct')
let wrong = document.getElementById('wrong')
let valid = /^[+]{1}[380]{3}[0-9]{9}/g;

function validate(){
    if(valid.test(input.value)){
        correct.style.display = 'block'
    } else{
        wrong.style.display = 'block'
        document.getElementById('buttonSub').disabled = true;
        input.style.border = 'solid red'
    }
}

/* END TASK 2 */

/* START TASK 3: Your code goes here */

let court = document.getElementById('court')
let ball = document.getElementById('ball')
let scoreA = document.getElementById('scoreA')
let scoreB = document.getElementById('scoreB')
let anounceA = document.getElementById('announceA')
let anounceB = document.getElementById('announceB')
let a = document.getElementById('A')
let b = document.getElementById('B')

let countA = 0;
let countB = 0;

court.onclick = (e) => {
    ball.style.position = 'absolute'
    let x = e.pageX - ball.offsetWidth/2 ;
    let y = e.pageY - ball.offsetHeight/2;
    ball.style.left = '0px';
    ball.style.top = '0px';
    ball.style.transform = `translate3d(${x}px, ${y}px, 0)`
}

scoreA.onclick = (e) => {
  countA++
  a.innerHTML = countA;
  anounceA.style.display = 'block';  
  setTimeout(() => {
    anounceA.style.display = 'none';
  }, 3000);
  ball.style.position = 'absolute'
  let x = e.pageX - ball.offsetWidth/2 ;
  let y = e.pageY - ball.offsetHeight/2;
  ball.style.left = '0px';
  ball.style.top = '0px';
  ball.style.transform = `translate3d(${x}px, ${y}px, 0)`
  
}

scoreB.onclick = (e) => {
  countB++
  b.innerHTML = countB;
  anounceB.style.display = 'block'   
  setTimeout(() => {
    anounceB.style.display = 'none';
  }, 3000);   
  ball.style.position = 'absolute'
  let x = e.pageX - ball.offsetWidth/2 ;
  let y = e.pageY - ball.offsetHeight/2;
  ball.style.left = '0px';
  ball.style.top = '0px';
  ball.style.transform = `translate3d(${x}px, ${y}px, 0)`
  
}

/* END TASK 3 */
