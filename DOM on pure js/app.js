const data = [
  {
    folder: true,
    title: 'Grow',
    children: [
      {
        title: 'logo.png'
      },
      {
        clicked: false,
        folder: true,
        title: 'English',
        children: [
          {
            title: 'Present_Perfect.txt'
          }
        ]
      }
    ]
  },
  {
    folder: true,
    title: 'Soft',
    children: [
      {
        folder: true,
        title: 'NVIDIA',
        children: [{ title: 'Folder is empty' }]
      },
      {
        title: 'nvm-setup.exe'
      },
      {
        title: 'node.exe'
      }
    ]
  },
  {
    folder: true,
    title: 'Doc',
    children: [
      {
        title: 'project_info.txt'
      }
    ]
  },
  {
    title: 'credentials.txt'
  }
];

(function createMenu() {
  let wrap = document.createElement('div');
  wrap.classList.add('context-menu');
  let option1 = document.createElement('p');
  let option2 = document.createElement('p');
  document.querySelector('#root').append(wrap);
  option1.textContent = 'rename';
  option1.id = 'rename';
  option2.id = 'deleteItem';
  wrap.appendChild(option1);
  option2.textContent = 'delete item';
  wrap.appendChild(option2);
})();

let root = document.getElementById('root');
let target = document.getElementById('target');
let rename = document.getElementById('rename');
let deleteItem = document.getElementById('deleteItem');

function renderDom(data) {
  const list = document.createElement('ul');

  for (let i = 0; i < data.length; i++) {
    const item = document.createElement('li');
    const wrap = document.createElement('div');
    const text = document.createElement('p');
    text.textContent = data[i].title;
    item.appendChild(wrap);
    wrap.appendChild(text);
    item.classList.add(data[i].folder ? 'folder' : 'file');

    if (data[i].folder) {
      wrap.classList.add('wrap');
      wrap.insertAdjacentHTML(
        'afterbegin',
        '<i class="material-icons fold">folder</i>'
      );
    } else if (
      data[i].title !== 'Folder is empty' &&
      data[i].folder === undefined
    ) {
      wrap.classList.add('wrapFile');
      wrap.insertAdjacentHTML(
        'afterbegin',
        '<span class="material-icons">insert_drive_file</span>'
      );
    }

    list.appendChild(item);

    if (data[i].folder && data[i].children !== null) {
      item.appendChild(renderDom(data[i].children)).classList.add('child');
    } else if (data[i]['title'] === 'Folder is empty') {
      item.style.fontStyle = 'italic';
    }
  }

  return list;
}

const tree = renderDom(data);
tree.classList.add('tree');
root.appendChild(tree);

for (let i = 0; i < document.querySelectorAll('.wrap').length; i++) {
  let child = document.querySelectorAll('.child');
  child[i].style.display = 'none';
  document.querySelectorAll('.wrap')[i].addEventListener('click', () => {
    if (child[i].style.display === 'none') {
      document.querySelectorAll('.fold')[i].innerHTML = 'folder_open';
      child[i].style.display = 'block';
    } else if (child[i].style.display === 'block') {
      child[i].style.display = 'none';
      document.querySelectorAll('.fold')[i].innerHTML = 'folder';
    }
  });
}

root.addEventListener('click', () => {
  document.querySelector('.context-menu').style.display = 'none';
});

root.addEventListener(
  'contextmenu',
  function (e) {
    console.log(e.target);
    e.preventDefault();
    let x = e.clientX;
    let y = e.clientY;
    document.querySelector('.context-menu').style.left = x + 'px';
    document.querySelector('.context-menu').style.top = y + 'px';
    document.querySelector('.context-menu').style.display = 'block';
    deleteItem.addEventListener('click', () => {
      if (e.target !== 'Folder is empty') {
        const empty = document.createElement('p');
        empty.innerHTML = 'Folder is empty';
        empty.style.fontStyle = 'italic';
        e.target.parentNode.firstChild.innerHTML = '';
        e.target.innerHTML = '';
        e.target.appendChild(empty);
        console.log(e.target.parentNode);
      }
    });
    rename.addEventListener('click', () => {
      let input = document.createElement('input');
      input.value = e.target.innerHTML;
      e.target.innerHTML = '';
      e.target.appendChild(input);
      input.select();
      input.setSelectionRange(0, input.value.indexOf('.'));
      input.addEventListener('click', () => {
        e.target.innerHTML = input.value;
        input.remove();
      });
    });
  },
  false
);
