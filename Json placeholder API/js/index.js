/* eslint-disable no-magic-numbers */
// Your code goes here

window.onload = async function getUsers() {
    const users = await fetch('https://jsonplaceholder.typicode.com/users')
        .then((response) => response.json())
    users.forEach(user => {
        let div = document.createElement('div');
        div.classList.add('userData');
        div.id = `${user.id}`;
        let html = `
          <div class="wrap">
          <table>
          <tr>
          <td >name</td>
          <td >city</td>
          <td >phone</td>
          <td >company name</td>
          <td >email</td>
          </tr>
            <tr>
            <td class='name name${user.id}'>${user.name}</td>
            <td contenteditable='true' id='city'>${user.address.city}</td>
            <td contenteditable='true' id='phone'>${user.phone}</td>
            <td contenteditable='true' id='company'>${user.company.name}</td>
            <td contenteditable='true' id='email'>${user.email}</td>
            <td>
                <button type="button" class="delete delete${user.id}">Delete user</button>
            </td>
            <td>
                <button type="button" class="edit edit${user.id}">Edit user</button>
            </td>
            </tr>
          </table>
          </div>
      `;
        div.innerHTML += html;
        document.getElementById('list').appendChild(div);



        let userpost = document.createElement('div');
        userpost.classList.add('userPost');
        div.appendChild(userpost)
        userpost.style.display = 'none'


        const spinner = document.getElementById('spinner');


        function showSpinner() {
            spinner.className = 'show';
            setTimeout(() => {
                spinner.className = spinner.className.replace('show', '');
            }, 1000);
        }


        document.querySelector(`.edit${user.id}`).addEventListener('click', () => {
            showSpinner()
            fetch(`https://jsonplaceholder.typicode.com/users/${user.id}`, {
                method: 'PUT',
                body: JSON.stringify({
                    name: `${document.getElementById('name').innerHTML}`,
                    city: `${document.getElementById('city').innerHTML}`,
                    phone: `${document.getElementById('phone').innerHTML}`,
                    email: `${document.getElementById('email').innerHTML}`
                }),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8'
                }
            })
                .then((response) => response.json())
                .then((json) => console.log(json))
        })
        document.querySelector(`.delete${user.id}`).addEventListener('click', () => {
            showSpinner()
            fetch(`https://jsonplaceholder.typicode.com/users/${user.id}`, {
                method: 'DELETE'
            })
            div.remove();
        })
        document.querySelector(`.name${user.id}`).addEventListener('click', async () => {
            showSpinner()

            if (userpost.style.display === 'none') {
                userpost.style.display = 'block'

                let postTitle = await fetch(`https://jsonplaceholder.typicode.com/posts/${user.id}`)
                    .then((response) => response.json())


                let comments = await fetch(`https://jsonplaceholder.typicode.com/posts/${user.id}/comments`)
                    .then((response) => response.json())

                let innerhtml = `
         <div style='border: solid 1px black'>
             <span > Post: ${postTitle.title}</span>
             <span > Post: ${postTitle.body}</span>
         </div>  
         <div style='border: solid 1px black'>    
             <p>${comments[0].name}</p>
             <p>${comments[1].name}</p>
             <p>${comments[2].name}</p>
             <p>${comments[3].name}</p>
             <p>${comments[4].name}</p>
         </div> 
         `
                userpost.innerHTML = innerhtml
            } else {
                userpost.style.display = 'none'
            }
        });
    })
}